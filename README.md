# nirvana-stylelint

An opinionated set of rules for for [stylelint]. There are many like it, but this one is mine.

[stylelint]: https://stylelint.io/

## Usage

- Install the package

  ```sh
  pnpm add -D https://gitlab.com/whatisaphone/nirvana-stylelint/-/archive/version/nirvana-stylelint-version.tar.gz
  ```

- Reference it in `stylelint.config.js`:

  ```js
  // https://stylelint.io/user-guide/configure

  const nirvana = require('nirvana-stylelint');

  const config = nirvana.base();
  // Add any config relevant to your project. Here are the choices:
  nirvana.addSCSS(config);
  nirvana.addCSSModules(config);
  // Optionally, add other stylelint config relevant to your project:
  nirvana.merge(config, {
    rules: {
      // ...
    },
  });
  module.exports = config;
  ```

- Add a script to `package.json`:

  ```json
  {
    "scripts": {
      "lint": "pnpm lint:css **/*.scss",
      "lint:css": "stylelint"
    }
  }
  ```

- Add a hook to `.pre-commit-config.yaml`:

  ```yaml
  - id: lint:css
    name: lint:css
    files: '[.]scss$'
    language: system
    entry: pnpm lint:css --fix
  ```

## Plugins

Legend:

- ✅ Included by default
- ➕ Included with extra method
- ⬅️ Included transitively
- ❓ Under consideration
- ❌ Rejected

Configs:

- ➕ [stylelint-config-css-modules] – with `addCSSModules`
- ❌ [stylelint-config-prettier] – deprecated since Stylelint 15
- ⬅️ [stylelint-config-recommended] – included via `stylelint-config-recommended-scss`
- ✅ [stylelint-config-recommended-scss]
- ✅ [stylelint-config-sass-guidelines]
- ✅ [stylelint-config-standard]
- ❌ [stylelint-config-standard-scss] – this is just whitespace rules and naming conventions

Plugins:

- ❌ [stylelint-a11y] – unmaintained
- ❌ [stylelint-at-rule-import-path] – doesn't work with latest stylelint
- ❌ [stylelint-at-rule-no-children] – Nice in theory, but makes code layout too jumbled
- ❌ [stylelint-at-rule-no-debug]
- ❌ [stylelint-color-format]
- ➕ [stylelint-css-modules]
- ❌ [stylelint-csstree-validator] – [does not handle SCSS well](https://github.com/csstree/stylelint-validator/issues)
- ✅ [stylelint-declaration-block-no-ignored-properties]
- ❌ [stylelint-declaration-strict-value] – almost works great, but doesn't handle shorthand well, and fails on multi-value declarations like `box-shadow`
- ❌ [stylelint-declaration-use-variable] – looks like a less-maintained version of `stylelint-declaration-strict-value`
- ❌ [stylelint-disallow-selector]
- ✅ [stylelint-high-performance-animation]
- ❌ [stylelint-images] – unmaintained
- ❌ [stylelint-known-property] – moved to stylelint core as `property-no-unknown`
- ❌ [stylelint-media-use-custom-media] – prefer SCSS-based solution
- ❌ [stylelint-no-browser-hacks] – It isn't 2001 anymore
- ✅ [stylelint-no-indistinguishable-colors]
- ❌ [stylelint-no-px] – Nice in theory, but I'm unsure whether this is actually a best practice
- ❌ [stylelint-no-restricted-syntax]
- ❌ [stylelint-no-undoing-styles] – Not nuanced enough. Doesn't handle `body { margin: 0 }` or `.foo { position: absolute; left: 0 }`
- ❌ [stylelint-no-unsupported-browser-features] – Good in theory, but mostly catches things like `box-shadow` which are purely aesthetic, and "partially unsupported" things which I'm using the fully supported subset of
- ❌ [stylelint-no-unused-selectors] – does not handle [any extension for CSS modules except `.css`](https://github.com/nodaguti/stylelint-no-unused-selectors/blob/86dd202eed34493781071fe1493f9fb2bc9d64fb/src/plugins/stylelint-no-unused-selectors-plugin-tsx/stylelint-no-unused-selectors-plugin-tsx.ts#L173)
- ❌ [stylelint-number-z-index-constraint]
- ❓ [stylelint-order]
- ❌ [stylelint-performance-animation] – Possibly abandoned, and `stylelint-high-performance-animation` has a more complete list of properties
- ❌ [stylelint-property-unknown] – does the same thing as stylelint's `property-no-unknown`
- ✅ [stylelint-require-units]
- ❌ [@signal-noise/stylelint-scales] – grid system I don't use
- ⬅️ [stylelint-scss] – used transitively by `stylelint-config-sass-guidelines`
- ❌ [stylelint-selector-no-mergeable] – doesn't work with latest stylelint
- ✅ [stylelint-selector-pseudo-class-lvhfa]
- ❌ [stylelint-selector-qualifying-element] – does the same thing as stylelint core `selector-no-qualifying-type`
- ❌ [stylelint-selector-tag-no-without-class]
- ❌ [stylelint-use-logical] – Spec is still a draft. I'll revisit later.
- ❌ [stylelint-use-nesting] – This is for [CSS Nesting](https://drafts.csswg.org/css-nesting-1/), not SCSS nesting. They are slightly different.
- ❌ [stylelint-value-border-zero]
- ✅ [stylelint-value-list-box-shadow-inset-first]
- ❌ [stylelint-value-no-padding-auto]
- ✅ [stylelint-value-no-unknown-custom-properties]
- ❌ [stylelint-value-shorthand] – does the same as stylelint core `shorthand-property-no-redundant-values`
- ❌ [stylelint-z-index-value-constraint]

[stylelint-a11y]: https://github.com/YozhikM/stylelint-a11y
[stylelint-at-rule-import-path]: https://github.com/timothyneiljohnson/stylelint-at-rule-import-path
[stylelint-at-rule-no-children]: https://github.com/adityavm/stylelint-at-rule-no-children
[stylelint-at-rule-no-debug]: https://github.com/timothyneiljohnson/stylelint-at-rule-no-debug
[stylelint-color-format]: https://github.com/filipekiss/stylelint-color-format
[stylelint-config-css-modules]: https://github.com/pascalduez/stylelint-config-css-modules
[stylelint-config-prettier]: https://github.com/prettier/stylelint-config-prettier
[stylelint-config-recommended]: https://github.com/stylelint/stylelint-config-recommended
[stylelint-config-recommended-scss]: https://github.com/kristerkari/stylelint-config-recommended-scss
[stylelint-config-sass-guidelines]: https://github.com/bjankord/stylelint-config-sass-guidelines
[stylelint-config-standard]: https://github.com/stylelint/stylelint-config-standard
[stylelint-config-standard-scss]: https://github.com/stylelint-scss/stylelint-config-standard-scss
[stylelint-css-modules]: https://github.com/juanca/stylelint-css-modules
[stylelint-csstree-validator]: https://github.com/csstree/stylelint-validator
[stylelint-declaration-block-no-ignored-properties]: https://github.com/kristerkari/stylelint-declaration-block-no-ignored-properties
[stylelint-declaration-strict-value]: https://github.com/AndyOGo/stylelint-declaration-strict-value
[stylelint-declaration-use-variable]: https://github.com/sh-waqar/stylelint-declaration-use-variable
[stylelint-disallow-selector]: https://github.com/azu/stylelint-disallow-selector
[stylelint-high-performance-animation]: https://github.com/kristerkari/stylelint-high-performance-animation
[stylelint-images]: https://github.com/ramasilveyra/stylelint-images
[stylelint-known-property]: https://github.com/GiG/stylelint-known-property
[stylelint-media-use-custom-media]: https://github.com/csstools/stylelint-media-use-custom-media
[stylelint-no-browser-hacks]: https://github.com/Slamdunk/stylelint-no-browser-hacks
[stylelint-no-indistinguishable-colors]: https://github.com/ierhyna/stylelint-no-indistinguishable-colors
[stylelint-no-px]: https://github.com/meowtec/stylelint-no-px
[stylelint-no-restricted-syntax]: https://github.com/niksy/stylelint-no-restricted-syntax
[stylelint-no-undoing-styles]: https://github.com/florianuphoff/stylelint-no-undoing-styles
[stylelint-no-unsupported-browser-features]: https://github.com/ismay/stylelint-no-unsupported-browser-features
[stylelint-no-unused-selectors]: https://github.com/nodaguti/stylelint-no-unused-selectors
[stylelint-number-z-index-constraint]: https://github.com/niksy/stylelint-number-z-index-constraint
[stylelint-order]: https://github.com/hudochenkov/stylelint-order
[stylelint-performance-animation]: https://github.com/konstantin24121/stylelint-performance-animation
[stylelint-property-unknown]: https://github.com/timothyneiljohnson/stylelint-property-unknown
[stylelint-require-units]: https://github.com/matterialize/stylelint-require-units
[@signal-noise/stylelint-scales]: https://github.com/signal-noise/stylelint-scales
[stylelint-scss]: https://github.com/kristerkari/stylelint-scss
[stylelint-selector-no-mergeable]: https://github.com/timothyneiljohnson/stylelint-selector-no-mergeable
[stylelint-selector-pseudo-class-lvhfa]: https://github.com/niksy/stylelint-selector-pseudo-class-lvhfa
[stylelint-selector-qualifying-element]: https://github.com/timothyneiljohnson/stylelint-selector-qualifying-element
[stylelint-selector-tag-no-without-class]: https://github.com/Moxio/stylelint-selector-tag-no-without-class
[stylelint-use-logical]: https://github.com/csstools/stylelint-use-logical
[stylelint-use-nesting]: https://github.com/csstools/stylelint-use-nesting
[stylelint-value-border-zero]: https://github.com/timothyneiljohnson/stylelint-value-border-zero
[stylelint-value-list-box-shadow-inset-first]: https://github.com/niksy/stylelint-value-list-box-shadow-inset-first
[stylelint-value-no-padding-auto]: https://github.com/timothyneiljohnson/stylelint-value-no-padding-auto
[stylelint-value-no-unknown-custom-properties]: https://github.com/csstools/stylelint-value-no-unknown-custom-properties
[stylelint-value-shorthand]: https://github.com/timothyneiljohnson/stylelint-value-shorthand
[stylelint-z-index-value-constraint]: https://github.com/kristerkari/stylelint-z-index-value-constraint

### Order

awaiting https://github.com/cahamilton/css-property-sort-order-smacss/issues/4

- https://github.com/stormwarning/stylelint-config-recess-order
- https://github.com/constverum/stylelint-config-rational-order
- https://github.com/cahamilton/stylelint-config-property-sort-order-smacss
- https://github.com/ream88/stylelint-config-idiomatic-order
- https://github.com/chaucerbao/stylelint-config-concentric-order
- https://www.npmjs.com/package/stylelint-logical-order
- https://github.com/jefferson-william/stylelint-config-rational-declaration

## Development

### Install the pre-commit hook

```sh
pre-commit install
```

This installs a Git hook that runs a quick sanity check before every commit.

### Cutting a release

- Push to master

- Wait for CI to pass

- Edit version in `package.json`

- Run:

```sh
git add package.json
version=$(<package.json jq -r .version)
git commit -m"v${version}"
git push
git tag "v${version}"
git push origin "v${version}"
```
