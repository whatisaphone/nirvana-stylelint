// https://stylelint.io/user-guide/configuration

const mergeWith = require('lodash.mergewith');
const stylelintConfigSassGuidelines = require('stylelint-config-sass-guidelines');

const camelCase = '[a-z][a-zA-Z0-9]*';

const containsColorVerbose = String.raw`
  #
|
  \b(?:rgba?|hsla?|hwb|lab|lch|oklab|oklch|color)
  [(]
  (?!
    (?:[a-zA-Z0-9-]+[.])?
    [$]
  )`;
const containsColor = containsColorVerbose.replaceAll(/\s/g, '');
const containsVariableOrKeyword =
  '[$]|^(currentColor|inherit|initial|none|revert|transparent|unset)$';

exports.base = base;
function base() {
  /*
   * stylelint
   * https://stylelint.io/user-guide/configure
   * https://stylelint.io/user-guide/rules/list
   */
  const config = {
    reportNeedlessDisables: true,
    reportInvalidScopeDisables: true,
    rules: {
      'declaration-no-important': true,
      'declaration-property-value-disallowed-list': {
        background: `/${containsColor}/`,
        '/^border/': `/${containsColor}/`,
        'box-shadow': `/${containsColor}/`,
        color: [`/${containsColor}/`, 'currentColor'],
        '/^(.*-)color$/': `/${containsColor}/`,
        fill: `/${containsColor}/`,
        outline: `/${containsColor}/`,
        stroke: `/${containsColor}/`,
        'text-shadow': `/${containsColor}/`,
        transition: String.raw`\ball\b`,
      },
      'declaration-property-value-allowed-list': {
        '/^font(-family)?$/': `/${containsVariableOrKeyword}/`,
        'z-index': `/${containsVariableOrKeyword}/`,
      },
      'unit-disallowed-list': ['em'],
    },
  };

  /*
   * stylelint-config-standard
   * https://github.com/stylelint/stylelint-config-standard
   */
  merge(config, {
    extends: ['stylelint-config-standard'],
  });

  /*
   * stylelint-config-recommended-scss
   * https://github.com/stylelint-scss/stylelint-config-recommended-scss
   */
  merge(config, {
    extends: ['stylelint-config-recommended-scss'],
  });

  /*
   * stylelint-config-sass-guidelines
   * https://github.com/bjankord/stylelint-config-sass-guidelines
   */
  merge(config, {
    extends: ['stylelint-config-sass-guidelines'],
    rules: {
      'max-nesting-depth': [
        stylelintConfigSassGuidelines.rules['max-nesting-depth'][0],
        mergeWith(
          { ...stylelintConfigSassGuidelines.rules['max-nesting-depth'][1] },
          { ignore: ['pseudo-classes'] },
        ),
      ],
    },
  });

  /*
   * stylelint-declaration-block-no-ignored-properties
   * https://github.com/kristerkari/stylelint-declaration-block-no-ignored-properties
   */
  merge(config, {
    plugins: ['stylelint-declaration-block-no-ignored-properties'],
    rules: {
      'plugin/declaration-block-no-ignored-properties': true,
    },
  });

  /*
   * stylelint-high-performance-animation
   * https://github.com/kristerkari/stylelint-high-performance-animation
   */
  merge(config, {
    plugins: ['stylelint-high-performance-animation'],
    rules: {
      'plugin/no-low-performance-animation-properties': [
        true,
        { ignore: 'paint-properties' },
      ],
    },
  });

  /*
   * stylelint-no-indistinguishable-colors
   * https://github.com/ierhyna/stylelint-no-indistinguishable-colors
   */
  merge(config, {
    plugins: ['stylelint-no-indistinguishable-colors'],
    rules: {
      'plugin/stylelint-no-indistinguishable-colors': true,
    },
  });

  /*
   * stylelint-order
   * https://github.com/hudochenkov/stylelint-order
   */
  merge(config, {
    plugins: ['stylelint-order'],
    rules: {
      // TODO
    },
  });

  /*
   * stylelint-require-units
   * https://github.com/matterialize/stylelint-require-units
   */
  merge(config, {
    plugins: ['stylelint-require-units'],
    rules: {
      'matterialize/stylelint-require-units': true,
    },
  });

  /*
   * stylelint-selector-pseudo-class-lvhfa
   * https://github.com/niksy/stylelint-selector-pseudo-class-lvhfa
   */
  merge(config, {
    plugins: ['stylelint-selector-pseudo-class-lvhfa'],
    rules: {
      'plugin/selector-pseudo-class-lvhfa': true,
    },
  });

  /*
   * stylelint-value-list-box-shadow-inset-first
   * https://github.com/niksy/stylelint-value-list-box-shadow-inset-first
   */
  merge(config, {
    plugins: ['stylelint-value-list-box-shadow-inset-first'],
    rules: {
      'plugin/value-list-box-shadow-inset-first': true,
    },
  });

  /*
   * stylelint-value-no-unknown-custom-properties
   * https://github.com/csstools/stylelint-value-no-unknown-custom-properties
   */
  merge(config, {
    plugins: ['stylelint-value-no-unknown-custom-properties'],
    rules: {
      'csstools/value-no-unknown-custom-properties': true,
    },
  });

  return config;
}

exports.addCSSModules = addCSSModules;
function addCSSModules(config) {
  /*
   * stylelint-config-css-modules
   * https://github.com/pascalduez/stylelint-config-css-modules
   */
  merge(config, {
    extends: ['stylelint-config-css-modules'],
    rules: {
      // Use camelCase so classes can be easily imported from JS.
      'selector-class-pattern': [
        `^${camelCase}$`,
        {
          message:
            'Selector should be written in camel case (selector-class-pattern)',
        },
      ],
    },
  });

  /*
   * stylelint-css-modules
   * https://github.com/juanca/stylelint-css-modules
   */
  merge(config, {
    plugins: ['stylelint-css-modules'],
    rules: {
      'css-modules/composed-class-names': true,
      'css-modules/css-variables': [
        true,
        { resolve: { extensions: ['.css', '.scss'] } },
      ],
    },
  });
}

/**
 * Merge stylelint configs in a reasonable way.
 */
exports.merge = merge;
function merge(...sources) {
  mergeWith(...sources, (objValue, srcValue) => {
    // Arrays get concatenated (`extends`, `plugins`, etc.)
    if (Array.isArray(objValue)) {
      objValue.push(...srcValue);
      return objValue;
    }
    // For anything else, fall back to Lodash's default recursive merge.
    return undefined;
  });
}
