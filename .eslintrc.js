// https://eslint.org/docs/user-guide/configuring

const nirvana = require('nirvana-eslint');

const config = nirvana.base();
module.exports = config;
